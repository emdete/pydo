# Dockerfile
#
FROM i386/debian:buster

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -y update && \
	apt-get -y install python3 && \
	rm -rf /var/lib/apt/lists/*

COPY . /data

VOLUME [ "/data" ]
